from __future__ import unicode_literals

import os

from documents.tests import TEST_SMALL_DOCUMENT_FILENAME

TEST_SMALL_DOCUMENT_PATH = os.path.join(
    'contrib', 'sample_documents',
    TEST_SMALL_DOCUMENT_FILENAME
)
